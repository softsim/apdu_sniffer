#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if len(sys.argv) != 3:
    print("i need 2 args: infile and outfile")
    exit(-1)

infile  = sys.argv[1]
outfile = sys.argv[2]

i = 0
with open(outfile, 'w') as outfile, open(infile, 'r', encoding='utf-8') as infile:
    for line in infile:
        e = line.split(',')
        i = i + 1
        try:
            outfile.write(e[2][2:4] + ' ')
        except IndexError as xxx:
            print(e)
            print(xxx)
            raise Exception('wori')

        if i % 16 == 0:
            outfile.write('\n')
