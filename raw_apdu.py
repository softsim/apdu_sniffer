#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import binascii
import os

if len(sys.argv) != 2:
    print("i need 1 args: apdu.bin")
    exit(-1)




file_maps = {
    b'\x2F\x00': 'Application Directory',
    b'\x2F\x05': 'Preferred Languages',
    b'\x2F\x06': 'Access Rule Reference',
    b'\x2F\xE2': 'ICCID',
    
    b'\x3F\x00': 'MF',


    b'\x4F\x09': 'PBC(Phonebook Control)',
    b'\x4F\x0A': 'PBC1(Phonebook Control)',    
    b'\x4F\x11': 'Phone book ANRA(Additional Number)',
    b'\x4F\x12': 'Phone book ANRA1(Additional Number)',
    b'\x4F\x13': 'Phone book ANRB(Additional Number)',
    b'\x4F\x14': 'Phone book ANRB1(Additional Number)',    
    b'\x4F\x15': 'Phone book ANRC', 
    b'\x4F\x19': 'SNE',
    b'\x4F\x20': 'GSM Ciphering key Kc',
    b'\x4F\x21': 'Phone book Unique ID',
    b'\x4F\x22': 'Phone book PSC(Sync Counter)',
    b'\x4F\x23': 'Phone book CC(Change Counter)',
    b'\x4F\x24': 'Phone book PUID(Previous UID)',
    b'\x4F\x25': 'Phone book GRP1',
    b'\x4F\x26': 'Phone book GRP',    
    b'\x4F\x30': 'Phone book Reference file',

    b'\x4F\x3A': 'Phone book Abbreviated dialling numbers',
    b'\x4F\x3B': 'Phone book ADN1',
    b'\x4F\x4A': 'Phone book EXT1',
    b'\x4F\x4B': 'Phone book Additional number Alpha String',
    b'\x4F\x4C': 'Phone book Grouping information Alpha String',
    b'\x4F\x50': 'Phone book EMAIL',
    b'\x4F\x51': 'Phone book EMAIL1',

    b'\x4F\x52': 'GPRS Ciphering key KcGPRS',


    b'\x5F\x3A': 'DF_PHONEBOOK',
    b'\x5F\x3B': 'DF_GSM_ACCESS',
    b'\x5F\x3C': 'DF_MMSS (CDMA)',

    b'\x6F\x05': 'Language Indication',
    b'\x6F\x06': 'Access Rule Reference',
    b'\x6F\x07': 'IMSI',
    b'\x6F\x08': 'Ciphering and Integrity keys',
    b'\x6F\x09': 'C and I keys for pkt switched domain',
    b'\x6F\x11': 'GSM_CPHS Voice Mail Waiting Indicator',
    b'\x6F\x13': 'GSM_CPHS Call Forwarding Flag',
    b'\x6F\x14': 'GSM_CPHS Operator Name String',
    b'\x6F\x15': 'GSM_CPSH Customer Service Profile',
    b'\x6F\x16': 'GSM_CPHS Information',
    b'\x6F\x17': 'GSM_CPHS Mailbox Number',
    b'\x6F\x18': 'EF ON Shortform',
    b'\x6F\x20': 'Ciphering key Kc',
    b'\x6F\x30': 'PLMN selector',
    b'\x6F\x31': 'Higher Priority PLMN search period',
    b'\x6F\x37': 'ACM maximum value',
    b'\x6F\x38': 'SIM/USIM Service Table',
    b'\x6F\x39': 'Accumulated call meter',
    b'\x6F\x3A': 'TELECOM_ADN(Abbreviated dialling numbers)',
    b'\x6F\x3B': 'TELECOM_FDN(Fixed dialling numbers)',
    b'\x6F\x3C': 'TELECOM Short messages',
    b'\x6F\x3D': 'Capability Configuration Parameter 1',
    b'\x6F\x3E': 'Group Identifier Level 1',
    b'\x6F\x3F': 'Group Identifier Level 2',
    b'\x6F\x40': 'TELECOM MSISDN',
    b'\x6F\x42': 'TELECOM Short message service parameters',
    b'\x6F\x43': 'TELECOM SMS status',
    b'\x6F\x44': 'Last number dialled',
    b'\x6F\x45': 'Cell broadcast message identifier selection',
    b'\x6F\x46': 'Service Provider Name',
    b'\x6F\x47': 'Short message status reports',
    b'\x6F\x48': 'Cell Broadcast Message Identifier for Data Download',
    b'\x6F\x49': 'Service Dialling Numbers',
    b'\x6F\x4A': 'Extension1',
    b'\x6F\x4B': 'Extension2',
    b'\x6F\x4C': 'Extension3',
    b'\x6F\x4E': 'Extension5',
    b'\x6F\x4F': 'Capability Configuration Parameters 2',    
    b'\x6F\x56': 'Enabled Services Table',
    b'\x6F\x5B': 'Initialisation values for Hyperframe number',
    b'\x6F\x5C': 'Maximum value of START',
    b'\x6F\x60': 'User controlled PLMN Selector with Access Technology',
    b'\x6F\x61': 'Operator controlled PLMN selector with Access Technology',
    b'\x6F\x62': 'HPLMN Selector with Access Technology',
    b'\x6F\x65': '!!! Deprecated EF!!!',
    b'\x6F\x73': 'Packet Switched location information',
    b'\x6F\x78': 'Access Control Class',
    b'\x6F\x7B': 'Forbidden PLMNs',
    b'\x6F\x7E': 'Location Information',
    b'\x6F\x80': 'Incoming Call Information',
    b'\x6F\x81': 'Outgoing Call Information',
    b'\x6F\xAD': 'Administrative Data',
    b'\x6F\xAE': 'GSM Phase Identification',
    b'\x6F\xB7': 'Emergency Call Codes',
    b'\x6F\xC3': 'Key for hidden phone book entries',
    b'\x6F\xC4': 'Network Parameters',
    b'\x6F\xCB': 'Call Forwarding Indication Status',
    b'\x6F\xD9': 'Equivalent HPLMN',
    b'\x6F\xE3': 'EPS location information',
    b'\x6F\xE4': 'EPS NAS Security Context',

    
    b'\x7F\x10': 'DF_Telecom',
    b'\x7F\x20': 'DF_GSM',    
    b'\x7F\x25': 'DF_CDMA',
}

#Multimode System Selection
cdma_mmss_efs = {
    b'\x4F\x20': 'MMSS Location Associated Priority List',
    b'\x4F\x21': 'MMSS System Priority List',
    b'\x4F\x22': 'MMSS Mode Settings'
}

csim_efs = {
    b'\x6F\x06': 'Access Rule Reference',

    b'\x6F\x21': 'Call Count',
    b'\x6F\x22': 'IMSI_M',
    b'\x6F\x23': 'IMSI_T',
    b'\x6F\x24': 'TMSI',
    b'\x6F\x25': 'Analog Home SID',
    b'\x6F\x28': 'CDMA Home SID, NID',
    b'\x6F\x29': 'CDMA Zone-Based Registration Indicators',
    b'\x6F\x2A': 'CDMA System-Network Registration Indicators',
    b'\x6F\x2B': 'CDMA Distance-Based Registration Indicators',
    b'\x6F\x2C': 'Access Overload Class ACCOLCp',
    b'\x6F\x2D': 'Call Termination Mode Preferences',
    b'\x6F\x2E': 'Suggested Slot Cycle Index',

    b'\x6F\x30': 'Preferred Roaming List',
    b'\x6F\x31': 'Removable UIM_ID',
    b'\x6F\x32': 'CSIM Service Table',
    b'\x6F\x34': 'OTAPA/SPC_Enabled',
    b'\x6F\x35': 'NAM_LOCK',
    b'\x6F\x36': 'OTASP/OTAPA Features',
    b'\x6F\x37': 'Service Preferences',
    b'\x6F\x3A': 'Language Indication',
    b'\x6F\x3B': 'Fixed Dialling Numbers',
    b'\x6F\x3C': 'Short Messages',
    b'\x6F\x3D': 'Short Message Service Parameters',
    b'\x6F\x3E': 'SMS Status',
    b'\x6F\x41': 'CDMA Home Service Provider Name',
    b'\x6F\x42': 'UIM_ID/SF_EUIMID Usage Indicator',
    b'\x6F\x43': 'Administrative Data',
    b'\x6F\x44': 'Mobile Directory Number',
    b'\x6F\x45': 'Maximum PRL',
    b'\x6F\x46': 'SPC Status',
    b'\x6F\x47': 'Emergency Call Codes',
    b'\x6F\x48': 'ME 3GPD Operation Capability',
    b'\x6F\x49': '3GPD Operation Mode',
    b'\x6F\x4A': 'Simple IP Capability Parameters',
    b'\x6F\x4C': 'Simple IP User Profile Parameters',
    b'\x6F\x4D': 'Mobile IP User Profile Parameters',


    b'\x6F\x55': 'ME-specific Configuration Request Parameters',
    b'\x6F\x56': 'HRPD Access Authentication Capability Parameters',
    b'\x6F\x57': 'HRPD Access Authentication User Profile Parameters',
    b'\x6F\x5A': 'Extended Preferred Roaming List',
    
    b'\x6F\x75': 'Enabled Service Table',
    b'\x6F\x76': 'Key for hidden phone book entrie',

    b'\x6F\x7B': 'Extension 3',
    b'\x6F\x7E': 'Extension 5',

    b'\x6F\x80': 'Application Labels',
    b'\x6F\x81': 'Device Model Information',
    b'\x6F\x83': 'SMS Capabilities',
    b'\x6F\x85': '3GPD User Profile Parameters Extension',
    b'\x6F\x89': 'Data Generic Configurations',

}


valid_command_class = (0xA0, 0x00, 0x80)

USIM_SFI = (
    b'\x6F\xB7',  b'\x6F\x05',  b'\x6F\xAD',  b'\x6F\x28',
    b'\x6F\x56',  b'\x6F\x78',  b'\x6F\x07',  b'\x6F\x08',
    b'\x6F\x09',  b'\x6F\x60',  b'\x6F\x7E',  b'\x6F\x73',
    b'\x6F\x7B',  b'\x6F\x48',  b'\x6F\x5B',  b'\x6F\x5C',

    b'\x6F\x61',  b'\x6F\x31',  b'\x6F\x62',  b'\x6F\x80',
    b'\x6F\x81',  b'\x6F\x4F',  b'\x6F\x06',  b'\x6F\xE4',
    b'\x6F\xC5',  b'\x6F\xC6',  b'\x6F\xCD',  b'\x6F\x39',
    b'\x6F\xD9',  b'\x6F\xE3' 
    )

CSIM_SFI = (
    b'\x6F\x43',  b'\x6F\x32',  b'\x6F\x2C',  b'\x6F\x22',
    b'\x6F\x23',  b'\x6F\x24',  b'\x6F\x30',  b'\x6F\x41',
    b'\x6F\x47',  b'\x6F\x3A',  b'\x6F\x6B',  b'\x6F\x28',
    b'\x6F\x2A',  b'\x6F\x5A',  b'\x6F\x75',  b'\x6F\x7C',
    b'\x6F\x7D',  b'\x6F\x7F'
    )

ISIM_SFI = (
    b'\xFF\xFF',  b'\x6F\x02',  b'\x6F\xAD',  b'\x6F\x04',
    b'\x6F\x03',  b'\x6F\x06',  b'\x6F\x07',  b'\xFF\xFF',
    b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',
    b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',
    b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',  b'\xFF\xFF',
    b'\xFF\xFF',  b'\xFF\xFF',  b'\x6F\x06'
)



INS_Select_File   = 0xA4
INS_Get_Status    = 0xF2
INS_Read_Binary   = 0xB0
INS_Update_Binary = 0xD6
INS_Read_Record   = 0xB2
INS_Update_Record = 0xDC
INS_Search_Record = 0xA2
INS_CHANGE_PIN  = 0x24
INS_Unblock_Pin = 0x2C
INS_Verify_Pin  = 0x20
INS_Authenticate      = 0x88
INS_Terminal_Profile  = 0x10
INS_Envelope          = 0xC2
INS_Fetch             = 0x12
INS_Terminal_Response = 0x14
INS_Manage_Channel   = 0x70
INS_Get_Response      = 0xC0
INS_Get_Data          = 0xCA
INS_Put_Data          = 0xDA
INS_Terminal_Capability = 0xAA
INS_Store_Data          = 0xE2

INS_Store_ESN_MEID_ME         = 0XDE
INS_Generate_KEY_VPM          = 0x8E
INS_Manage_SSD                = 0x82
INS_Base_Station_Challenge    = 0x8A
INS_Compute_IP_Authentication = 0x80
INS_UPDATE_SSD                = 0x84

INS_SEP_MAPS = (INS_Select_File, INS_Terminal_Profile, INS_Terminal_Response, INS_Fetch, INS_Envelope,
    INS_Authenticate, INS_Base_Station_Challenge, INS_Generate_KEY_VPM,
    INS_Manage_Channel,  INS_Unblock_Pin,  INS_Store_ESN_MEID_ME,  INS_Get_Status)


def print_hex(x, prefix='', comments=''):
    print(prefix + binascii.hexlify(x).decode('ASCII').upper() + comments)


with open(sys.argv[1], 'rb') as binfile:
    while True:
        apdu = binfile.read(5)
        if len(apdu) < 5:
            break

        if apdu[1] in INS_SEP_MAPS:
            print(' ')

        if apdu[1] == INS_Read_Binary or apdu[1] == INS_Update_Binary:
            if apdu[2] & 0x80 != 0:
                print(' ')

        if apdu[1] == INS_Read_Record or apdu[1] == INS_Update_Record:
            if apdu[3] >> 3 != 0:
                print(' ')

        print_hex(apdu)

        cls = apdu[0] & 0xFC
        chs = apdu[0] & 0x03
        if cls not in valid_command_class:
            print('Invalid Class')
            print('%x' % cls)
            print('%x' % valid_command_class[0])
            break



        if apdu[1] == INS_Select_File:
            select_ack = binfile.read(1)  ## ack byte

            if select_ack[0] == 0x60:
                select_ack = binfile.read(1)

            if select_ack[0] == 0x6E:
                print('invalid cls')
                binfile.read(1)
            else:
                file_id = binfile.read(apdu[4])

                if apdu[2] == 0x04 and apdu[3] == 0x04:
                    print_hex(file_id,   comments='           ### <aid>')
                else:
                    if len(file_id) == 4:
                        orig_file_id = file_id
                        file_id = file_id[-2:]
                        if chs == 0:
                            if file_id in file_maps:
                                print_hex(orig_file_id, comments='   ### USIM EF: '+ file_maps[file_id])
                            else:
                                print_hex(orig_file_id)
                        else:
                            if file_id in csim_efs:
                                print_hex(orig_file_id, comments='  ### CSIM EF: ' + csim_efs[file_id])
                            else:
                                print_hex(orig_file_id)
                    elif len(file_id) == 6:
                        df = file_id[2:4]
                        ef = file_id[4:]
                        orig_file_id = file_id
                        commentz = '              '
                        if df in file_maps:
                            commentz = commentz + file_maps[df]
                        #if df == b'\x5F\x3A':
                        #    if ef in phonebook_ef:
                        #        print(phonebook_ef[ef] + ' :: ' + binascii.hexlify(ef).decode('ASCII').upper())
                        #else:
                        if ef in file_maps:
                            commentz = commentz + file_maps[ef]

                        print_hex(orig_file_id,  comments=commentz)

                    else:
                        if file_id in file_maps:
                            print_hex(file_id,  comments='   #### ' + file_maps[file_id])
                        else:
                            print_hex(file_id)


                while True:
                    if binfile.read(1)[0] == 0x60:   ### wait
                        pass
                    else:
                        binfile.seek(-1, 1)
                        break

                response = binfile.read(2)
                print_hex(response, prefix='>>>  ')



        elif apdu[1] == INS_Get_Response:
            myack = binfile.read(1)
            if myack == b'\xc0':
                if apdu[4] == 0:
                    xxx = binfile.read(256+2)
                    print('-------read data-----')
                    print_hex(xxx)
                else:
                    fcp = binfile.read(apdu[4]+2)
                    print_hex(fcp, prefix='(fcp)>>> ')
            else:
                zzz = binfile.read(1)
                print_hex(myack + zzz, '#### Exception')

        elif apdu[1] == INS_Read_Binary:
            commentz = ''
            if apdu[2] & 0x80 != 0:
                sfi = apdu[2] & 0x7F
                commentz = '                      ### '
                #-------------------------
                if chs == 0:
                    file_id = USIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()             
                    if file_id in file_maps:
                        commentz = commentz + '  --usim--  ' + file_maps[file_id]    + '   sfi: %x' % (sfi)
                else:
                    #file_id = CSIM_SFI[sfi-1]
                    file_id = ISIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()                      
                    if file_id in csim_efs:
                        commentz = commentz + '  --csim--  ' + csim_efs[file_id]     + '   sfi: %x' % (sfi)
                #-----------------------

            ack = binfile.read(1)[0]
            if ack != INS_Read_Binary:
                bbb = binfile.read(1)
                print(('%02x%02x' % (ack, bbb[0])),  '   ### Not Found. OR other reason')
            else:
                data_len = apdu[4]
                if data_len == 0:
                    data_len = 256
                data = binfile.read(data_len+2)
                print_hex(data, prefix='(bin)>>> ',  comments = commentz)

        elif apdu[1] == INS_Terminal_Profile:
            binfile.read(1)
            profile = binfile.read(apdu[4])
            while binfile.read(1)[0] == 0x60:
                pass
            yyy = binfile.read(1)
            print_hex(profile,   comments='    ###  Terminal Profile ')
  

        elif apdu[1] == INS_Read_Record:
            ack = binfile.read(1)
            if ack[0] != INS_Read_Record:
                bbb = binfile.read(1)
                print_hex(ack+bbb,  comments='   ###  Record Not Found')
            else:

                sfi = apdu[3] >> 3
                if sfi != 0:
                    commentz = '       '
                    if chs == 0:
                        file_id = USIM_SFI[sfi-1]
                        commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()
                        if file_id in file_maps:
                            commentz = commentz + ' -usim- ' + file_maps[file_id]  + '   sfi: %x' % (sfi)
                    else:
                        #file_id = CSIM_SFI[sfi-1]
                        print("sfi=%02x,%02x" % (sfi,  apdu[3]))
                        try:
                            file_id = ISIM_SFI[sfi-1]
                            commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()
                            if file_id in csim_efs:
                                commentz = commentz + ' -csim--- ' + csim_efs[file_id]    + '   sfi: %x' % (sfi)
                        except:
                            pass
                else:
                    commentz = ''

                record = binfile.read(apdu[4]+2)
                print_hex(record,  prefix='(rec)>>> ',  comments=commentz)

        elif apdu[1] == INS_Manage_Channel:
            if apdu[2] == 0x80:
                print_hex(binfile.read(2), prefix='>>> ',    comments='  ### close logical channel ')
            else:
                ack = binfile.read(1)[0]
                if ack != INS_Manage_Channel:
                    print('### not support logic channel')
                    binfile.read(1)
                else:
                    print_hex(binfile.read(3), prefix='>>> ',   comments='  ### open logical channel ')

        elif apdu[1] == INS_Update_Binary:
            binfile.read(1)

            if apdu[2] & 0x80 != 0:
                sfi = apdu[2] & 0x7F
                 #-------------------------
                commentz = '        ###  update binary: '
                if chs == 0:
                    file_id = USIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()
                    if file_id in file_maps:
                        commentz = commentz + ' -usim- ' + file_maps[file_id]  + '   sfi: %x' % (sfi)
                else:
                    #file_id = CSIM_SFI[sfi-1]
                    file_id = ISIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()                    
                    if file_id in csim_efs:
                        commentz = commentz + ' -csim--- ' + csim_efs[file_id]    + '   sfi: %x' % (sfi)
                #-----------------------
            else:
                commentz = ''

            update_bin = binfile.read(apdu[4])
            print_hex(update_bin,  comments=commentz)
            
            while binfile.read(1)[0] == 0x60:
                pass
            binfile.read(1)

        elif apdu[1] == INS_Authenticate:
            binfile.read(1)
            auth_bin = binfile.read(apdu[4])
            print_hex(auth_bin, comments='    ###  --------auth----------')
            while binfile.read(1)[0] == 0x60:
                pass
            rsp_len = binfile.read(1)

        elif apdu[1] == INS_Get_Status:
            sw = binfile.read(1)[0]
            while sw == 0x60:
                sw = binfile.read(1)[0]


            if sw == 0x6D:
                binfile.read(1)
                print('invalid status inst')
            else:
                if apdu[3] == 0x0C or apdu[4] == 0:
                    binfile.read(1)                ##  9000
                else:
                    status = binfile.read(apdu[4]+2)
                    print_hex(status, prefix='>>> ')

        elif apdu[1] == INS_Unblock_Pin or apdu[1] == INS_Verify_Pin or apdu[1] == INS_CHANGE_PIN:
            ack_pin = binfile.read(1)[0]
            if ack_pin == INS_Unblock_Pin or ack_pin == INS_Verify_Pin or ack_pin == INS_CHANGE_PIN:
                xxx = binfile.read(apdu[4])
                print_hex(xxx,  prefix='ADM OR PUK OR PIN>>> ')
            else:
                binfile.seek(-1, 1)

            print_hex(binfile.read(2), prefix='>>> ',  comments='  ### pin or puk')


        elif apdu[1] == INS_Search_Record:
            binfile.read(1)
            seek_data =  binfile.read(apdu[4])
            print_hex(seek_data)
            while binfile.read(1)[0] == 0x60:
                pass
            binfile.read(1)

        elif apdu[1] == INS_Envelope:
            binfile.read(1)
            envelope = binfile.read(apdu[4])
            print_hex(envelope, comments='       ### ENVELOPE')
            print_hex(binfile.read(2), prefix='>>>')

        elif apdu[1] == INS_Update_Record:
            binfile.read(1)

            sfi = apdu[3] >> 3
            if sfi != 0:
                commentz = '###  update record: '
                if chs == 0:
                    file_id = USIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()
                    if file_id in file_maps:
                        commentz = commentz + ' -usim- ' + file_maps[file_id]  + '   sfi: %x' % (sfi)
                else:
                    #file_id = CSIM_SFI[sfi-1]
                    file_id = ISIM_SFI[sfi-1]
                    commentz = commentz + binascii.hexlify(file_id).decode('ASCII').upper()
                    if file_id in csim_efs:
                        commentz = commentz + ' -csim--- ' + csim_efs[file_id]    + '   sfi: %x' % (sfi)

                print(commentz)

            update_record = binfile.read(apdu[4])
            print_hex(update_record)

            while binfile.read(1)[0] == 0x60:
                pass
            binfile.read(1)

        elif apdu[1] == INS_Fetch or apdu[1] == INS_Terminal_Response:
            binfile.read(1)
            cap = binfile.read(apdu[4])
            if apdu[1] == INS_Fetch:
                print_hex(cap,  comments='       ### Fetch')
            else:
                print_hex(cap,  comments='       ### Terminal_Response')

            while (binfile.read(1)[0] == 0x60):
                pass
            zzz = binfile.read(1)
            print_hex(zzz, prefix='>>> ')

        elif apdu[1] == INS_Store_ESN_MEID_ME:
            binfile.read(1)
            meid = binfile.read(apdu[4])
            print_hex(meid,  comments='       ## store meid')
            zzz = binfile.read(2)
            print_hex(zzz, prefix='>>> ')

        elif apdu[1] == INS_Generate_KEY_VPM:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx,  comments='     ###----gen key vpm')
            zzz = binfile.read(2)
            print_hex(zzz, prefix='>>> ')

        elif apdu[1] == INS_Manage_SSD:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx,   comments='    ###----gen or save ssd')
            zzz = binfile.read(2)
            print_hex(zzz, prefix='>>> ')
            
        elif apdu[1] == INS_Base_Station_Challenge:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx,  comments='    ###---base station challenge---')
            zzz = binfile.read(2)
            print_hex(zzz, prefix='>>> ')

        elif apdu[1] == INS_Compute_IP_Authentication:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx,  comments='      ### ---compute ip authentication---')
            zzz = binfile.read(2)
            print_hex(zzz, prefix='>>> ')

        elif apdu[1] == INS_UPDATE_SSD:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx, comments='       ### update ssd')
            while binfile.read(1)[0] == 0x60:
                pass
            binfile.read(1)

        elif apdu[1] == INS_Get_Data:
            if apdu[4] == 0:
                binfile.read(2)
            else:
                xxx = binfile.read(apdu[4] + 3)
                print_hex(xxx,  prefix='>>> ')

        elif apdu[1] == INS_Terminal_Capability:
            binfile.read(1)
            xxx = binfile.read(apdu[4]+2)
            print_hex(xxx,  comments='      ### ---terminal capability---')

        elif apdu[1] == INS_Store_Data:
            binfile.read(1)
            xxx = binfile.read(apdu[4])
            print_hex(xxx,  comments='      ### ---euicc store data---')
            zzz = binfile.read(2)

        else:
            print('Invalid Instruction')
            break
        

