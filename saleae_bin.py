#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, binascii

if len(sys.argv) != 3:
    print("i need 2 args: infile and outfile")
    exit(-1)

infile  = sys.argv[1]
outfile = sys.argv[2]

i = 0
with open(outfile, 'wb') as outfile, open(infile, 'r', encoding='utf-8') as infile:
    for line in infile:
        e = line.split(',')
        i = i + 1
        x = binascii.unhexlify(e[2][2:4])
        outfile.write(x)
