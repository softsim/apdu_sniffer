#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if len(sys.argv) != 2:
    print("i need 2 args: log.txt")
    exit(-1)

infile  = sys.argv[1]

with  open(infile, 'r', encoding='utf-8') as infile:
    for line in infile:
        rstart = line.find('receive apdu is ')
        if rstart != -1:
            capdu = line[rstart+16:]
            print(capdu)
        else:
            sstart = line.find('send apdu is ')
            if sstart !=  -1:
                rapdu = line[sstart+12:]
                print(rapdu)
            
