#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if len(sys.argv) != 3:
    print("i need 2 args: infile and outfile")
    exit(-1)

infile  = sys.argv[1]
outfile = sys.argv[2]

j = 0
i = 0
with open(outfile, 'w') as outfile, open(infile, 'r', encoding='utf-8') as infile:
    for line in infile:
        j += 1
        if j%4 == 3:
            e = line.split(',')
            i = i + 1
            outfile.write(e[2][:2] + ' ')
            if i % 16 == 0:
                outfile.write('\n')
