#!/usr/bin/env python3
# -*- coding: utf-8 -*-

file_tags = {
    0x80 : 'Size',
    0x81 : 'Length',
    0x82 : 'File Descriptor',
    0x83 : 'File Identifier',
    0x84 : 'DF Name',
    0x85 : 'Proprietary no-BERTLV',
    0x86 : 'Proprietary Security Attribute',
    0x87 : 'EF with FCI extension',
    0x88 : 'Short File Identifier',
    0x8A : 'Life Cycle Status',
    0x8B : 'Security Attributes ref to expanded',
    0x8C : 'Security Attributes compact',
    0x8D : 'EF with Security Environment',
    0x8E : 'Channel Security Attribute',
    0xA0 : 'Security Attribute for DO',
    0xA1 : 'Proprietary Security Attribute',
    0xA2 : 'DO Pairs',
    0xA5 : 'Proprietary BERTLV',
    0xAB : 'Security Attribute expanded',
    0xC6 : 'PIN Status',
    }     

# from python 2.6, format('b') allows to use 0b10010110 notation: 
# much convenient
def byteToBit(byte):
    '''
    byteToBit(0xAB) -> [1, 0, 1, 0, 1, 0, 1, 1]
    
    converts a byte integer value into a list of bits
    '''
    bit = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(8):
        if byte % pow(2, i+1):
            bit[7-i] = 1
            byte = byte - pow(2, i)
    return bit



def first_TLV_parser(bytelist):
    '''
    first_TLV_parser([0xAA, 0x02, 0xAB, 0xCD, 0xFF, 0x00]) -> (170, 2, [171, 205])
    
    parses first TLV format record in a list of bytelist 
    returns a 3-Tuple: Tag, Length, Value
    Value is a list of bytes
    parsing of length is ETSI'style 101.220
    '''
    Tag = bytelist[0]
    if bytelist[1] == 0xFF:
        Len = bytelist[2]*256 + bytelist[3]
        Val = bytelist[4:4+Len]
    else:
        Len = bytelist[1]
        Val = bytelist[2:2+Len]
    return (Tag, Len, Val)

def TLV_parser(bytelist):
    '''
    TLV_parser([0xAA, ..., 0xFF]) -> [(T, L, [V]), (T, L, [V]), ...]
    
    loops on the input list of bytes with the "first_TLV_parser()" function
    returns a list of 3-Tuples
    '''
    ret = []
    while len(bytelist) > 0:
        T, L, V = first_TLV_parser(bytelist)
        if T == 0xFF:
            # padding bytes
            break
        ret.append( (T, L, V) )
        # need to manage length of L
        if L > 0xFE: 
            bytelist = bytelist[ L+4 : ]
        else: 
            bytelist = bytelist[ L+2 : ]
    return ret



def first_BERTLV_parser(bytelist):
    '''
    first_BERTLV_parser([0xAA, 0x02, 0xAB, 0xCD, 0xFF, 0x00]) 
        -> ([1, 'contextual', 'constructed', 10], [1, 2], [171, 205])
    
    parses first BER-TLV format record in a list of bytes
    returns a 3-Tuple: Tag, Length, Value
        Tag: [Tag class, Tag DO, Tag number]
        Length: [Length of length, Length value]
        Value: [Value bytes list]
    parsing of length is ETSI'style 101.220
    '''
    # Tag class and DO
    byte0 = byteToBit(bytelist[0])
    if byte0[0:2] == [0, 0]:
        Tag_class = 'universal'
    elif byte0[0:2] == [0, 1]:
        Tag_class = 'applicative'
    elif byte0[0:2] == [1, 0]:
        Tag_class = 'contextual'
    elif byte0[0:2] == [1, 1]:
        Tag_class = 'private'
    if byte0[2:3] == [0]:
        Tag_DO = 'primitive'
    elif byte0[2:3] == [1]:
        Tag_DO = 'constructed'
    # Tag coded with more than 1 byte
    i = 0
    if byte0[3:8] == [1, 1, 1, 1, 1]:
        Tag_bits = byteToBit(bytelist[1])[1:8]
        i += 1
        while byteToBit(bytelist[i])[0] == 1:
            i += 1
            Tag_bits += byteToBit(bytelist[i])[1:8]
    # Tag coded with 1 byte
    else:
        Tag_bits = byte0[3:8]
    
    # Tag number calculation
    Tag_num = 0
    for j in range(len(Tag_bits)):
        Tag_num += Tag_bits[len(Tag_bits)-j-1] * pow(2, j)
    
    # Length coded with more than 1 byte (BER long form)
    if bytelist[i+1] & 0x80:
        Len_num = bytelist[i+1] - 0x80
        Len = reduce(lambda x,y: (x<<8)+y, bytelist[i+2:i+2+Len_num])
        Val = bytelist[i+2+Len_num:i+2+Len_num+Len]
    
    # Length coded with 1 byte (BER short form)
    else:
        Len_num = 1
        Len = bytelist[i+1]
        Val = bytelist[i+2:i+2+Len]

    return ([i+1, Tag_class, Tag_DO, Tag_num], [Len_num, Len], Val)
#=============================================================
def BERTLV_parser(bytelist):
    '''
    BERTLV_parser([0xAA, ..., 0xFF]) -> [([T], L, [V]), ([T], L, [V]), ...]
    
    loops on the input bytes with the "first_BERTLV_parser()" function
    returns a list of 3-Tuples containing BERTLV records
    '''
    ret = []
    while len(bytelist) > 0:
        T, L, V = first_BERTLV_parser(bytelist)
        #if T == 0xFF: 
        #    break # padding bytes
        ret.append( (T[1:], L[1], V) )
        # need to manage lengths of Tag and Length
        bytelist = bytelist[ T[0] + L[0] + L[1] : ]
    return ret
#=================================================================

def parse_file(Data=[]):
    '''
    parse_file(self, Data) -> Dict()
    
    parses a list of bytes returned when selecting a file
    interprets the content of some informative bytes 
    for file structure and parsing method...
    '''
    ber = BERTLV_parser( Data )
    
    print('BER structure:\n%s' % ber)
    
    if len(ber) > 1:
        # TODO: implements recursive BER object parsing
        print('(parse_file) contain more than 1 BER object: %s\nnot implemented' % ber)
    
    # for FCP control structure, precise parsing is done
    # this structure seems to be the most used for (U)SIM cards
    if ber[0][0][2] == 0x2:
        fil = parse_FCP( ber[0][2] )
        fil['Control'] = 'FCP'
        return fil
    
    # for FCI control structure, also trying to parse precisely
    # this structure is used mainly in EMV cards
    elif ber[0][0][2] == 0x10:
        fil = parse_FCI( ber[0][2] )
        fil['Control'] = 'FCI'
        return fil
    
    # for other control structure, DIY
    fil = {}
    if ber[0][0][2] == 0x4: 
        fil['Control'] = 'FMD'
        print('(parse_file) FMD file structure parsing not implemented')
    elif ber[0][0][2] == 0xF:
        fil['Control'] = 'FCI'
        print('(parse_file) FCI 0xF file structure parsing not implemented')
    else: 
        fil['Control'] = ber[0][0]
        print('(parse_file) unknown file structure')

        
    fil['Data'] = ber[0][2]
    
    return fil
    
    
def parse_FCP(Data=[]):
    '''
    parse_FCP(Data) -> Dict()
    
    parses a list of bytes returned when selecting a file
    interprets the content of some informative bytes 
    for file structure and parsing method...
    '''
    fil = {}
    # loop on the Data bytes to parse TLV'style attributes
    toProcess = Data
    while len(toProcess) > 0:
        # TODO: seemd full compliancy 
        # would require to work with the BERTLV parser...
        [T, L, V] = first_TLV_parser(toProcess)

        if T in file_tags.keys(): 
            Tag = file_tags[T]
        else: 
            Tag = T
        print('(parse_FCP) Tag value %s / type %s: %s' % (T, Tag, V))
        
        # do extra processing here
        # File ID, DF name, Short file id
        if T in (0x83, 0x84, 0x88):
            fil[file_tags[T]] = V
        # Security Attributes compact format
        elif T == 0x8C:
            fil[file_tags[T]] = V
            fil = parse_compact_security_attribute(V, fil)
        # Security Attributes ref to expanded
        elif T == 0x8B:
            fil[file_tags[T]] = V 
            fil = parse_expanded_security_attribute(V, fil)
        # other security attributes... not implemented
        elif T in (0x86, 0x8E, 0xA0, 0xA1, 0xAB):
            fil[file_tags[T]] = V 
            # TODO: no concrete parsing at this time...
            print(2, '(parse_FCP) parsing security attributes not implemented for tag 0x%X' % T)
            fil = parse_security_attribute(V, fil)
        # file size or length
        elif T in (0x80, 0x81):
            fil[file_tags[T]] = sum( [ V[i] * pow(0x100, len(V)-i-1) \
                                           for i in range(len(V)) ] )
        # file descriptor, deducting file access, type and structure
        elif T == 0x82:
            assert( L in (2, 5) )
            fil[file_tags[T]] = V
            fil = parse_file_descriptor(V, fil)
        # life cycle status
        elif T == 0x8A:
            fil = parse_life_cycle(V, fil)
        # proprietary information
        elif T == 0xA5:
            fil = parse_proprietary(V, fil)
        else:
            if T in file_tags.keys():
                fil[file_tags[T]] = V
            else:
                fil[T] = V
        
        # truncate the data to process and loop
        if L < 256:
            toProcess = toProcess[L+2:]
        else:
            toProcess = toProcess[L+4:]
    
    # and return the file 
    return fil
    

def parse_life_cycle(Data, fil):
    '''
    parses a list of bytes provided in Data
    interprets the content as the life cycle
    and enriches the file dictionnary passed as argument
    '''
    if   Data[0] == 1: fil['Life Cycle Status'] = 'creation state'
    elif Data[0] == 3: fil['Life Cycle Status'] = 'initialization state'
    elif Data[0] in (5, 7): fil['Life Cycle Status'] = 'operational state' \
        ' - activated'
    elif Data[0] in (4, 6): fil['Life Cycle Status'] = 'operational state' \
        ' - deactivated'
    elif Data[0] in range(12, 15): fil['Life Cycle Status'] = \
        'termination state'
    elif Data[0] >= 16: fil['Life Cycle Status'] = 'proprietary'
    else: fil['Life Cycle Status'] = 'RFU'
    return fil
    

def parse_file_descriptor(Data, fil):
    '''
    parses a list of bytes provided in Data
    interprets the content as the file descriptor
    and enriches the file dictionnary passed as argument
    '''
    # parse the File Descriptor Byte
    fd = Data[0]
    fd_type = (fd >> 3) & 0b00111
    fd_struct = fd & 0b00000111
    # get Structure, Access and Type
    # bit b8
    if (fd >> 7) & 0b1: fil['Structure'] = 'RFU'
    # access bit b7
    if (fd >> 6) & 0b1: fil['Access'] = 'shareable'
    else              : fil['Access'] = 'not shareable'
    # structure bits b1 to b3
    if   fd_struct == 0: fil['Structure'] = 'no information'
    elif fd_struct == 1: fil['Structure'] = 'transparent'
    elif fd_struct == 2: fil['Structure'] = 'linear fixed'
    elif fd_struct == 3: fil['Structure'] = 'linear fixed TLV'
    elif fd_struct == 4: fil['Structure'] = 'linear variable'
    elif fd_struct == 5: fil['Structure'] = 'linear variable TLV'
    elif fd_struct == 6: fil['Structure'] = 'cyclic'
    elif fd_struct == 7: fil['Structure'] = 'cyclic TLV'
    else               : fil['Structure'] = 'RFU'
    # type bits b4 to b6
    if   fd_type == 0: fil['Type'] = 'EF working'
    elif fd_type == 1: fil['Type'] = 'EF internal'
    elif fd_type == 7: 
        fil['Type'] = 'DF'
        if   fd_struct == 1: fil['Structure'] = 'BER-TLV'
        elif fd_struct == 2: fil['Structure'] = 'TLV'
    else: fil['Type'] = 'EF proprietary'
    
    # for linear and cyclic EF: 
    # the following is convenient for UICC, 
    # but looks not fully conform to ISO standard
    # see coding convention in ISO 7816-4 Table 87
    if len(Data) == 5: 
        fil['Record Length'], fil['Record Number'] = Data[3], Data[4]
    
    return fil
    

def parse_proprietary(Data, fil):
    '''
    parses a list of bytes provided in Data
    interprets the content as the proprietary parameters
    and enriches the file dictionnary passed as argument
    '''
    propr_tags = {
        0x80:"UICC characteristics",
        0x81:"Application power consumption",
        0x82:"Minimum application clock frequency",
        0x83:"Amount of available memory",
        0x84:"File details",
        0x85:"Reserved file size",
        0x86:"Maximum file size",
        0x87:"Supported system commands",
        0x88:"Specific UICC environmental conditions",
        }
    while len(Data) > 0:
        [T, L, V] = first_TLV_parser( Data )
        if T in propr_tags.keys(): 
            fil[propr_tags[T]] = V
        Data = Data[L+2:]
    return fil
    

def parse_compact_security_attribute(Data, fil):
    '''
    parses a list of bytes provided in Data
    interprets the content as the compact form for security parameters
    and enriches the file dictionnary passed as argument
    '''
    # See ISO-IEC 7816-4 section 5.4.3, with compact and expanded format
    AM = Data[0]
    SC = Data[1:]
    sec = '#'
    
    # check access mode
    if 'Type' in fil.keys():
        # DF security attributes parsing
        if fil['Type'] == 'DF':
            sec += self._DF_access_mode(AM)
        # EF security attributes parsing
        else:
            sec += self._EF_access_mode(AM)
        # loop on security conditions for the given access mode:
        for cond in SC:
            sec += self._sec_cond(cond)
    # return security conditions if parsed, return raw bytes otherwise
    if sec == '#':
        fil['Security Attributes raw'] = Data
    else:
        fil['Security Attributes'] = sec
    return fil
    

def _DF_access_mode(AM):
    sec = ''
    if AM & 0b10000000 == 0:
        if AM & 0b01000000: sec += ' DELETE FILE #'
        if AM & 0b00100000: sec += ' TERMINATE DF #'
        if AM & 0b00010000: sec += ' ACTIVATE FILE #'
        if AM & 0b00001000: sec += ' DEACTIVATE FILE #'
    if AM & 0b00000100: sec += ' CREATE DF #'
    if AM & 0b00000010: sec += ' CREATE EF #'
    if AM & 0b00000001: sec += ' DELETE FILE #'
    return sec
    

def _EF_access_mode(AM):
    sec = ''
    if AM & 0b10000000 == 0:
        if AM & 0b01000000: sec += ' DELETE FILE #'
        if AM & 0b00100000: sec += ' TERMINATE EF #'
        if AM & 0b00010000: sec += ' ACTIVATE FILE #'
        if AM & 0b00001000: sec += ' DEACTIVATE FILE #'
    if AM & 0b00000100: sec += ' WRITE / APPEND #'
    if AM & 0b00000010: sec += ' UPDATE / ERASE #'
    if AM & 0b00000001: sec += ' READ / SEARCH #'
    return sec
        

def _Obj_access_mode(AM):
    sec = ''
    if AM & 0b00000100: sec += 'MANAGE SEC ENVIRONMENT #'
    if AM & 0b00000010: sec += 'PUT DATA'
    if AM & 0b00000001: sec += 'GET DATA'
    return sec
        

def _Tab_access_mode(val):
    sec = ''
    if AM & 0b10000000 == 0:
        if AM & 0b01000000: sec += ' CREATE / DELETE USER #'
        if AM & 0b00100000: sec += ' GRANT / REVOKE #'
        if AM & 0b00010000: sec += ' CREATE TABLE / VIEW / DICT #'
        if AM & 0b00001000: sec += ' DROP TABLE / VIEW #'
    if AM & 0b00000100: sec += ' INSERT #'
    if AM & 0b00000010: sec += ' UPDATE / DELETE #'
    if AM & 0b00000001: sec += ' FETCH #'
    return sec
    

def _sec_cond(cond):
    sec = ''
    if   cond == 0   : sec += ' Always #'
    elif cond == 0xff: sec += ' Never #'
    else:
        sec += ' SEID %s #' % (cond & 0b00001111)
        if cond & 0b10000000: sec += ' all conditions #'
        else: sec += ' at least 1 condition #'
        if cond & 0b01000000: sec += ' secure messaging #'
        if cond & 0b00100000: sec += ' external authentication #'
        if cond & 0b00010000: sec += ' user authentication #'
    return sec
    

def parse_expanded_security_attribute(Data, fil):
    '''
    check references to EF_ARR file containing access conditions
    see ISO 7816-4
    '''
    # self.ARR = {ARR_id:[ARR_content],...}
    return fil
    file_length = len(Data)
    if file_length == 1:
        ARR_byte = Data
    elif file_length == 3:
        ARR_ref = Data[0:2]
        ARR_byte = Data[2:3]
    elif file_length > 3:
        ARR_ref = Data[0:2]
        # handle SEID and ARR.byte in 2 // lists
        SEID_bytes, ARR_bytes = [], []
        # in case file_length is not even: truncate it...
        if file_length%2 == 1: file_length -= 1
        # parse SEID / ARR.bytes
        for i in range(2, file_length, 2):
            SEID_byte.append(Data[i:i+1])
            ARR_byte.append(Data[:+1:i+2])
        
    
    
def parse_security_attribute(Data, fil):
    '''
    TODO: to implement...
    
    need to work further on how to do it (with ref to EF_ARR)
    '''
    # See ISO-IEC 7816-4 section 5.4.3, with compact and expanded format
    # not implemented yet (looks like useless for (U)SIM card ?)
    return fil

#=================================================
def parse_FCI(Data=[]):
    '''
    parse_FCI(Data) -> Dict()
    
    parses a list of bytes returned when selecting a file
    interprets the content of some informative bytes 
    for file structure and parsing method...
    '''
    fil = {}
    # loop on the Data bytes to parse TLV'style attributes
    toProcess = Data
    while len(toProcess) > 0:
        # TODO: seemd full compliancy 
        # would require to work with the BERTLV parser...
        [T, L, V] = first_TLV_parser(toProcess)
        
        if T in file_tags.keys(): 
            Tag = file_tags[T]
        else: 
            Tag = T

        print('(parse_FCI) Tag value %s / type %s: %s' % (T, Tag, V))
        
        # application template
        if T == 0x61:
            fil['Application Template'] = V
        
        
        
        # do extra processing here
        # File ID, DF name, Short file id
        elif T in (0x83, 0x84, 0x88):
            fil[file_tags[T]] = V
        # Security Attributes compact format
        elif T == 0x8C:
            fil[file_tags[T]] = V
            fil = parse_compact_security_attribute(V, fil)
        # Security Attributes ref to expanded
        elif T == 0x8B:
            fil[self.file_tags[T]] = V 
            fil = parse_expanded_security_attribute(V, fil)
        # other security attributes... not implemented
        elif T in (0x86, 0x8E, 0xA0, 0xA1, 0xAB):
            fil[file_tags[T]] = V 
            # TODO: no concrete parsing at this time...
            print('(parse_FCP) parsing security attributes not implemented for tag 0x%X' % T)
            fil = parse_security_attribute(V, fil)
        # file size or length
        elif T in (0x80, 0x81):
            fil[file_tags[T]] = sum( [ V[i] * pow(0x100, len(V)-i-1) \
                                           for i in range(len(V)) ] )
        # file descriptor, deducting file access, type and structure
        elif T == 0x82:
            assert( L in (2, 5) )
            fil[file_tags[T]] = V
            fil = parse_file_descriptor(V, fil)
        # life cycle status
        elif T == 0x8A:
            fil = parse_life_cycle(V, fil)
        # proprietary information
        elif T == 0xA5:
            fil = parse_proprietary(V, fil)
        else:
            if T in file_tags.keys():
                fil[file_tags[T]] = V
            else:
                fil[T] = V
        
        # truncate the data to process and loop
        if L < 256:
            toProcess = toProcess[L+2:]
        else:
            toProcess = toProcess[L+4:]
    
    # and return the file 
    return fil


if __name__ == "__main__":
    import sys
        
    if len(sys.argv) != 2:
        print("i need 1 hexstr")
        exit(-1)

    fcp = sys.argv[1]
    import binascii      
    fcp_list =[x for x in binascii.unhexlify(fcp)]
    file_info = parse_file(fcp_list)
    print(file_info)
    
